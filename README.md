# Visual Studio Code - Code Editing. Redefined

Visual Studio Code is a code editor redefined and optimized for building and debugging modern web and cloud applications

## HomePage

[Visual Studio Code](https://code.visualstudio.com/)

## Download

Download **code-x86_64.AppImage** binary from here: [code-x86_64.AppImage](https://gitlab.com/linuxappimage/vscode/-/jobs/artifacts/main/raw/code-x86_64.AppImage?job=run-build)

## CLI

or from you command line:

```bash
curl -sLo code-x86_64.AppImage https://gitlab.com/linuxappimage/vscode/-/jobs/artifacts/main/raw/code-x86_64.AppImage?job=run-build

chmod +x code-x86_64.AppImage
./code-x86_64.AppImage

```

